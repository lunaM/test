package com.epam.vik100.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

import com.epam.vik100.model.ListTestReport;
import com.epam.vik100.model.ListTester;
import com.epam.vik100.model.SetTestReport;
import com.epam.vik100.model.SetTester;
import com.epam.vik100.model.TestReport;

public class TestingController {
	public static void main(String[] args) {		
		//testing speed of Collections that implemets List
		//ArrayList LinkedList Vector
		ListTestReport<Integer> listsToTest = new ListTestReport<>();
		List<Integer> arrayListTest = new ArrayList<>(100000);
		Collections.fill(arrayListTest, 1);
		arrayListTest.add(0,5);
		arrayListTest.add(arrayListTest.size()/2,5);
		arrayListTest.add(arrayListTest.size()/3,5);
		arrayListTest.add(arrayListTest.size(),5);
		listsToTest.add(new ListTester<Integer>(arrayListTest));
		List<Integer> linkedListTest = new LinkedList<>(arrayListTest);
		listsToTest.add(new ListTester<>(linkedListTest));		
		Vector<Integer> vectorTest = new Vector<>(arrayListTest);
		listsToTest.add(new ListTester<>(vectorTest));		
		for(ListTester<Integer> list: listsToTest.getTests()){
			list.findAddingTimeBegin(5);
			list.findAddingTimeCenter(5);
			list.findAddingTimeEnd(5);
			list.findRemovingTimeByPosBegin();
			list.findRemovingTimeByPosCenter();
			list.findRemovingTimeByPosEnd();
			list.findAddingTimeCenter(5);
			list.findFindingElementTime(5);
		}
		listsToTest.showReport();
		
		//testing speed of Collections that implemets Set
		//HashSet LinkedHashSet TreeSet
		SetTestReport<Integer> setsToTest = new SetTestReport<>();
		setsToTest.add(new SetTester<>(new HashSet<>(arrayListTest)));
		setsToTest.add(new SetTester<>(new LinkedHashSet<>(arrayListTest)));
		setsToTest.add(new SetTester<>(new TreeSet<>(arrayListTest)));
		for(SetTester<Integer> set: setsToTest.getTests()){
			set.findAddingTime(6);
			set.findFindingElementTime(6);
			set.findRemovingTime(6);
		}
		setsToTest.showReport();
	} 
}
