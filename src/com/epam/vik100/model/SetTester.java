package com.epam.vik100.model;

import java.util.Formatter;
import java.util.Set;

public class SetTester<T> {
	private Set<T> set;
	private String collectionName;
	private long addingTime;
	private long removingTime;
	private long findElementTime;

	public SetTester(Set<T> set) {
		this.set = set;
		this.collectionName = set.getClass().getSimpleName();
	}

	public long getFindElementTime() {
		return findElementTime;
	}

	public void setFindElementTime(long findElementTime) {
		this.findElementTime = findElementTime;
	}

	public Set<T> getList() {
		return set;
	}

	public long getAddingTime(T element) {
		long start = System.nanoTime();
		set.add(element);
		return System.nanoTime() - start;
	}

	public long getRemovingTime(T element) {
		long start = System.nanoTime();
		set.remove(element);
		return System.nanoTime() - start;
	}

	public long findFindingElementTime(T element) {
		long start = System.nanoTime();
		long result;
		set.contains(element);
		result = System.nanoTime() - start;
		findElementTime = result;
		return result;
	}

	public long findAddingTime(T element) throws UnsupportedOperationException {
		long result = getAddingTime(element);
		addingTime = result;
		return result;
	}

	public long findRemovingTime(T element) throws UnsupportedOperationException {
		long result = getRemovingTime(element);
		removingTime = result;
		return result;
	}

	private String convinientTime(long time) {
		return time + " ";
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\n|%-20s|%10s|%10s|%10s|", collectionName, convinientTime(addingTime),
				convinientTime(removingTime), convinientTime(findElementTime));
		return formatter.toString();
	}

	public String getCollectionName() {
		return collectionName;
	}

}
