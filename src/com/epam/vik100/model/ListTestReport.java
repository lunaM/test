package com.epam.vik100.model;

import java.util.ArrayList;
import java.util.List;

public class ListTestReport<T> implements TestReport{
	private List<ListTester<T>> tests;
	
	public List<ListTester<T>> getTests() {
		return tests;
	}

	public void setTests(List<ListTester<T>> tests) {
		this.tests = tests;
	}

	public ListTestReport() {
		tests = new ArrayList<>();
	}

	public void add(ListTester<T> test) {
		tests.add(test);
	}

	public void showReport() {
		System.out.println("\n\t\t\t*   *   *   THE RESULTS  listlike collections *   *   *\n");
		System.out.printf("|%-20s|%10s|%10s|%10s|%10s|%10s|%10s|%10s|%10s|%10s|%10s|", "Collection's name", "AtB",
				"AtC", "AtE", "RfBP", "RfCP", "RfEP", "RfBE", "RfCE", "RfEE", "FE");
		System.out.println();
		for (ListTester<T> test : tests) {
			System.out.println(test);
		}
	}
}
