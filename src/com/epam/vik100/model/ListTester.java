package com.epam.vik100.model;

import java.util.Formatter;
import java.util.List;

public class ListTester<T>{
	private List<T> list;
	private String collectionName;
	private long addingTimeBegin;
	private long addingTimeCentre;
	private long addingTimeEnd;
	private long removingTimeBeginByPos;
	private long removingTimeCentreByPos;
	private long removingTimeEndByPos;
	private long findElementTime;

	public ListTester(List<T> list) {
		this.list = list;
		this.collectionName = list.getClass().getSimpleName();
	}

	public List<T> getList() {
		return list;
	}

	public long getAddingTime(int pos, T element){
		long start = System.nanoTime();
		list.add(pos, element);
		return System.nanoTime() - start;
	}

	public long getRemovingTimeByPos(int pos){
		long start = System.nanoTime();
		list.remove(pos);
		return System.nanoTime() - start;
	}

	public long getRemovingTimeByElem(T element){
		long start = System.nanoTime();
		list.remove(element);
		return System.nanoTime() - start;
	}

	public long findFindingElementTime(T element){
		long start = System.nanoTime();
		long result;
		list.contains(element);
		result = System.nanoTime() - start;
		findElementTime = result;
		return result;
	}

	public long findAddingTimeBegin(T element){
		long result = getAddingTime(0, element);
		addingTimeBegin = result;
		return result;
	}

	public long findAddingTimeCenter(T element){
		long result = getAddingTime(list.size() / 2, element);
		addingTimeCentre = result;
		return result;
	}

	public long findAddingTimeEnd(T element){
		long result = getAddingTime(list.size(), element);
		addingTimeEnd = result;
		return result;
	}

	public long findRemovingTimeByPosBegin(){
		long result = getRemovingTimeByPos(0);
		removingTimeBeginByPos = result;
		return result;
	}

	public long findRemovingTimeByPosCenter(){
		long result = getRemovingTimeByPos(list.size() / 2);
		removingTimeCentreByPos = result;
		return result;
	}

	public long findRemovingTimeByPosEnd(){
		long result = getRemovingTimeByPos(list.size() - 1);
		removingTimeEndByPos = result;
		return result;
	}

	public String getCollectionName() {
		return collectionName;
	}

	private String convinientTime(long time) {
//		if (time == 0) {
//			return "-";
//		}
		return time + " ";
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\n|%-20s|%10s|%10s|%10s|%10s|%10s|%10s|%10s|", collectionName,
				convinientTime(addingTimeBegin), convinientTime(addingTimeCentre), convinientTime(addingTimeEnd),
				convinientTime(removingTimeBeginByPos), convinientTime(removingTimeCentreByPos),
				convinientTime(removingTimeEndByPos), convinientTime(findElementTime));
		return formatter.toString();
	}

	public long getAddingTimeBegin() {
		return addingTimeBegin;
	}

	public void setAddingTimeBegin(long addingTimeBegin) {
		this.addingTimeBegin = addingTimeBegin;
	}

	public long getAddingTimeCentre() {
		return addingTimeCentre;
	}

	public void setAddingTimeCentre(long addingTimeCentre) {
		this.addingTimeCentre = addingTimeCentre;
	}

	public long getAddingTimeEnd() {
		return addingTimeEnd;
	}

	public void setAddingTimeEnd(long addingTimeEnd) {
		this.addingTimeEnd = addingTimeEnd;
	}

	public long getRemovingTimeBeginByPos() {
		return removingTimeBeginByPos;
	}

	public void setRemovingTimeBeginByPos(long removingTimeBeginByPos) {
		this.removingTimeBeginByPos = removingTimeBeginByPos;
	}

	public long getRemovingTimeCentreByPos() {
		return removingTimeCentreByPos;
	}

	public void setRemovingTimeCentreByPos(long removingTimeCentreByPos) {
		this.removingTimeCentreByPos = removingTimeCentreByPos;
	}

	public long getRemovingTimeEndByPos() {
		return removingTimeEndByPos;
	}

	public void setRemovingTimeEndByPos(long removingTimeEndByPos) {
		this.removingTimeEndByPos = removingTimeEndByPos;
	}

	public long getFindElementTime() {
		return findElementTime;
	}

	public void setFindElementTime(long findElementTime) {
		this.findElementTime = findElementTime;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

}
