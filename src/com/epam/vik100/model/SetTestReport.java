package com.epam.vik100.model;

import java.util.ArrayList;
import java.util.List;

public class SetTestReport<T> implements TestReport{
	private List<SetTester<T>> tests;
	
	public List<SetTester<T>> getTests() {
		return tests;
	}

	public void setTests(List<SetTester<T>> tests) {
		this.tests = tests;
	}

	public SetTestReport() {
		tests = new ArrayList<>();
	}

	public void add(SetTester<T> test) {
		tests.add(test);
	}

	public void showReport() {
		System.out.println("\n\t\t\t*   *   *   THE RESULTS  setlike collections *   *   *\n");
		System.out.printf("|%-20s|%10s|%10s|%10s|", "Collection's name", "Add",
				"Remove", "Find");
		System.out.println();
		for (SetTester<T> test : tests) {
			System.out.println(test);
		}
	}
}
