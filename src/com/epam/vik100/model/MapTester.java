//package com.epam.vik100.model;
//
//import java.util.List;
//import java.util.Map;
//
//public class MapTester<F,T> implements Tester<T> {
//	private Map<EntrySetT> map;
//	private String collectionName;
//	private long addingTimeBegin;
//	private long addingTimeCentre;
//	private long addingTimeEnd;
//	private long removingTimeBeginByPos;
//	private long removingTimeCentreByPos;
//	private long removingTimeEndByPos;
//	private long findElementTime;
//
//	public MapTester(Map<T,F> map) {
//		this.map = map;
//		this.collectionName = map.getClass().getSimpleName();
//	}
//
//	public long getAddingTimeBegin() {
//		return addingTimeBegin;
//	}
//
//	public void setAddingTimeBegin(long addingTimeBegin) {
//		this.addingTimeBegin = addingTimeBegin;
//	}
//
//	public long getAddingTimeCentre() {
//		return addingTimeCentre;
//	}
//
//	public void setAddingTimeCentre(long addingTimeCentre) {
//		this.addingTimeCentre = addingTimeCentre;
//	}
//
//	public long getAddingTimeEnd() {
//		return addingTimeEnd;
//	}
//
//	public void setAddingTimeEnd(long addingTimeEnd) {
//		this.addingTimeEnd = addingTimeEnd;
//	}
//
//	public long getRemovingTimeBeginByPos() {
//		return removingTimeBeginByPos;
//	}
//
//	public void setRemovingTimeBeginByPos(long removingTimeBeginByPos) {
//		this.removingTimeBeginByPos = removingTimeBeginByPos;
//	}
//
//	public long getRemovingTimeCentreByPos() {
//		return removingTimeCentreByPos;
//	}
//
//	public void setRemovingTimeCentreByPos(long removingTimeCentreByPos) {
//		this.removingTimeCentreByPos = removingTimeCentreByPos;
//	}
//
//	public long getRemovingTimeEndByPos() {
//		return removingTimeEndByPos;
//	}
//
//	public void setRemovingTimeEndByPos(long removingTimeEndByPos) {
//		this.removingTimeEndByPos = removingTimeEndByPos;
//	}
//
//	public long getFindElementTime() {
//		return findElementTime;
//	}
//
//	public void setFindElementTime(long findElementTime) {
//		this.findElementTime = findElementTime;
//	}
//
//	public Map<T,F> getMap() {
//		return map;
//	}
//
//	public long getAddingTime(int pos, T element) throws UnsupportedOperationException {
//		long start = System.nanoTime();
//		list.add(pos, element);
//		return System.nanoTime() - start;
//	}
//
//	public long getRemovingTimeByPos(int pos) throws UnsupportedOperationException {
//		long start = System.nanoTime();
//		list.remove(pos);
//		return System.nanoTime() - start;
//	}
//
//	public long getRemovingTimeByElem(T element) throws UnsupportedOperationException {
//		long start = System.nanoTime();
//		list.remove(element);
//		return System.nanoTime() - start;
//	}
//
//	@Override
//	public long findFindingElementTime(T element) throws UnsupportedOperationException {
//		long start = System.nanoTime();
//		long result;
//		list.contains(element);
//		result = System.nanoTime() - start;
//		findElementTime = result;
//		return result;
//	}
//
//	@Override
//	public long findAddingTimeBegin(T element) throws UnsupportedOperationException {
//		long result = getAddingTime(0, element);
//		addingTimeBegin = result;
//		return result;
//	}
//
//	@Override
//	public long findAddingTimeCenter(T element) throws UnsupportedOperationException {
//		long result = getAddingTime(list.size() / 2, element);
//		addingTimeCentre = result;
//		return result;
//	}
//
//	@Override
//	public long findAddingTimeEnd(T element) throws UnsupportedOperationException {
//		long result = getAddingTime(list.size(), element);
//		addingTimeEnd = result;
//		return result;
//	}
//
//	@Override
//	public long findRemovingTimeByPosBegin() throws UnsupportedOperationException {
//		long result = getRemovingTimeByPos(0);
//		removingTimeBeginByPos = result;
//		return result;
//	}
//
//	@Override
//	public long findRemovingTimeByPosCenter() throws UnsupportedOperationException {
//		long result = getRemovingTimeByPos(list.size() / 2);
//		removingTimeCentreByPos = result;
//		return result;
//	}
//
//	@Override
//	public long findRemovingTimeByPosEnd() throws UnsupportedOperationException {
//		long result = getRemovingTimeByPos(list.size() - 1);
//		removingTimeEndByPos = result;
//		return result;
//	}
//
//	@Override
//	public String getCollectionName() {
//		return collectionName;
//	}
//
//}
